package utility;

import org.apache.log4j.Logger;

public class Log {

	// Initialize Log4j logs
	private static Logger Log = Logger.getLogger(Log.class.getName());

	// Print log for the beginning of the test case, as we usually run so many test cases as a test suite
	public static void startTestCase(String sTestCaseName) {

		Log.info("****************************************************************************************");

		Log.info("****************************************************************************************");

		Log.info("XXXXXXXXXXXXXXXXXXXXXXX            " + sTestCaseName + "            XXXXXXXXXXXXXXXXXXXXXXX");

		Log.info("****************************************************************************************");

		Log.info("****************************************************************************************");

	}

	// Print log for the ending of the test case
	public static void endTestCase(String sTestCaseName) {
		Log.info("XXXXXXXXXXXXXXXXXXXXXXX             " + "-E---N---D-" + "             XXXXXXXXXXXXXXXXXXXXXX");
	}

	// Print log for the starting of the test
	public static void startTest(String sTestCaseName) {
		Log.info("***********************            " + sTestCaseName + "            ***********************");
	}
	
	// Print log for the ending of the test 
	public static void endTest() {
		Log.info("****************************************************************************************");
	}
	
	
	public static void info(String message) {
		Log.info(message);
	}

	public static void warn(String message) {
		Log.warn(message);
	}

	public static void error(String message) {
		Log.error(message);
	}

	public static void fatal(String message) {
		Log.fatal(message);
	}

	public static void debug(String message) {
		Log.debug(message);
	}

}